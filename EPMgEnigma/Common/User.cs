﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EPMgEnigma.Common
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [JsonIgnore]
        public List<Contact> Contacts { get; set; }
        [JsonIgnore]
        public List<Invitations> Invitations { get; set; }

        public User()
        {

        }

        public User(
            string login, 
            string password)
        {
            this.Login = login;

            this.Password = password;
        }
    }
}
