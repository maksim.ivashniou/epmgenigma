﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace EPMgEnigma.Common
{
     public class Message
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ContactId { get; set; }

        [ForeignKey("ContactId")]
        [JsonIgnore]
        public Contact Contact { get; set; }

        public int CreatorId { get; set; }

        [ForeignKey("CreatorId")]
        [JsonIgnore]
        public User Creator { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public DateTime DateTime { get; set; }
    }
}
