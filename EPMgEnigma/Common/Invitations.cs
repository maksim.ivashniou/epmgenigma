﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace EPMgEnigma.Common
{
    public class Invitations
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int FromUserId { get; set; }

        [ForeignKey("FromUserId")]
        [JsonIgnore]
        public User FromUser { get; set; }

        [Required]
        public int ToUserId { get; set; }

        [ForeignKey("ToUserId")]
        [JsonIgnore]
        public User ToUser { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public string Key { get; set; }

        public string HelloMessage { get; set; }
    }
}

