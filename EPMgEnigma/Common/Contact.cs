﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EPMgEnigma.Common
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int FirstUserId { get; set; }

        [ForeignKey("FirstUserId")]
        public User FirstUser { get; set; }

        [Required]
        public int SecondUserId { get; set; }

        [ForeignKey("SecondUser")]
        public User SecondUser { get; set; }

        [Required]
        public string Key { get; set; }

        public List<Message> Messages { get; set; }
    }
}
