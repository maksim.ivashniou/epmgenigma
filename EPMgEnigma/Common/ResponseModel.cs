﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPMgEnigma.Common
{
    public class ResponseModel<T>
    {
        public bool IsSuccessful { get; set; }

        public string ErrorText { get; set; }

        public T Data { get; set; }

        private void InitResponse(
            bool isSuccessful,
            string errorText = null,
            T data = default)
        {
            IsSuccessful = isSuccessful;

            ErrorText = errorText;

            Data = data;
        }

        public ResponseModel(string errorText)
        {
            InitResponse(
                false,
                errorText,
                default);
        }

        public ResponseModel(T data)
        {
            InitResponse(true,
                null,
                data
                );
        }
    }
}
