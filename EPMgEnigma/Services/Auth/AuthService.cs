﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPMgEnigma.Common;
using EPMgEnigma.Data.Json.Users;

namespace EPMgEnigma.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly UserJsonHelper dataSource;

        public AuthService(UserJsonHelper dataSource)
        {
            this.dataSource = dataSource;
        }

        public ResponseModel<bool> IsLoginInDatabase(string login)
        {
            try
            {
                return new ResponseModel<bool>(data: dataSource.IsExist(login));
            }
            catch (Exception)
            {
                return new ResponseModel<bool>(errorText: "Current account login already exist");
            }
        }

        public User LogIn(string login, string password)
        {
            var users = dataSource.GetUsers().Data;

            return users.FirstOrDefault(user => user.Login.Equals(login) && user.Password.Equals(password));
        }

        public ResponseModel<bool> SignUp(string login, string password)
        {
            return dataSource.AddUser(new User(login, password));
        }
    }
}
