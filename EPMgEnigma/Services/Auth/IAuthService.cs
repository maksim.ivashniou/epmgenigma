﻿using System;
using System.Collections.Generic;
using System.Text;
using EPMgEnigma.Common;

namespace EPMgEnigma.Services.Auth
{
    public interface IAuthService
    {
        ResponseModel<bool> IsLoginInDatabase(string login);

        User LogIn(
            string login,
            string password);

        ResponseModel<bool> SignUp(
            string login, 
            string password);
    }
}
