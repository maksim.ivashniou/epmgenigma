﻿using EPMgEnigma.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EPMgEnigma.Data.Json.Users
{
    public class UserJsonHelper
    {
        private readonly string directoryPath;

        private readonly string usersDirectoryPath;

        private readonly string fileListOfUsersPath;

        private readonly List<User> users;

        public UserJsonHelper(string directoryPath)
        {
            this.directoryPath = directoryPath;

            this.usersDirectoryPath = $@"{this.directoryPath}\Data";

            this.fileListOfUsersPath = $@"{this.usersDirectoryPath}\Users.json";

            InitWorkDirectory();

            this.users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(this.fileListOfUsersPath));
        }

        private void InitWorkDirectory()
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (!Directory.Exists(usersDirectoryPath))
            {
                Directory.CreateDirectory(usersDirectoryPath);
            }

            if (!File.Exists(fileListOfUsersPath))
            {
                using (StreamWriter sw = new StreamWriter(fileListOfUsersPath, append: true))
                {
                    sw.Write(JsonConvert.SerializeObject(new List<User>()));
                }
            }
        }
        public ResponseModel<IReadOnlyCollection<User>> GetUsers()
        {
            try
            {
                return new ResponseModel<IReadOnlyCollection<User>>(data: users.AsReadOnly());
            }
            catch (Exception)
            {
                return new ResponseModel<IReadOnlyCollection<User>>(errorText: "oooops..."); // TODO write valid exception
            }
        }
        public ResponseModel<bool> AddUser(User nUser)
        {
            nUser.Id = users.Count();

            try
            {
                var result = JsonConvert.DeserializeObject<List<User>>(
                    File.ReadAllText(fileListOfUsersPath));

                if (result == null)
                {
                    result = new List<User>();
                }

                result.Add(nUser);

                File.WriteAllText(fileListOfUsersPath, JsonConvert.SerializeObject(result));

                users.Add(nUser);

                return new ResponseModel<bool>(data: true);
            }
            catch (Exception)
            {
                return new ResponseModel<bool>(errorText: "No access to file");
            }

        }
        public bool IsExist(string login)
        {
            return users.Any(user => user.Login.ToLower().Equals(login.ToLower()));
        }
    }
}
