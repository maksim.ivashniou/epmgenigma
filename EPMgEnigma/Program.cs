﻿using System;
using EPMgEnigma.UI;
using EPMgEnigma.Services.Auth;
using EPMgEnigma.Data.Json.Users;
using Microsoft.Extensions.DependencyInjection;

namespace EPMgEnigma
{
    class Program
    {
        static readonly string projectPath = @$"C:\Users\{Environment.UserName}\Documents\enigma";

        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton(new UserJsonHelper(projectPath))
                .AddTransient<IAuthService, AuthService>()
                .AddScoped<IEnigma, Enigma>()
                .BuildServiceProvider();

            serviceProvider
                .GetService<IEnigma>()
                .StartMenu();
        }
    }
}
