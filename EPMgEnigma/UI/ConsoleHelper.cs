﻿using System;

namespace EPMgEnigma.UI
{
    public class ConsoleHelper
    {
        public static string GetStringValue(
            string message = "Enter the string value",
            string error = "Not valid value")
        {
            do
            {
                WriteInfo(message);

                string value = Console.ReadLine();

                if (!string.IsNullOrEmpty(value) || !string.IsNullOrWhiteSpace(value))
                {
                    return value;
                }

                WriteInfo(error, ConsoleColor.Red);
            } while (true);
        }

        public static string GetLogin(
            string message = "Enter login",
            string error = "Login is not valid, please try again")
        {
            return GetStringValue(message, error);
        }

        public static string GetPassword(
            string message = "Enter password",
            string error = "Password is not valid, please try again"
            )
        {
            return GetStringValue(message, error);
        }

        public static KeyImput DrawStartMenuKeyCommand()
        {
            while (true)
            {
                ConsoleKeyInfo consoleKeyInfo = Console.ReadKey();

                WriteInfo("");

                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.D0:
                        {
                            return KeyImput.LogIn;
                        }
                    case ConsoleKey.D1:
                        {
                            return KeyImput.SignUp;
                        }
                    default:
                        {
                            WriteInfo("Command is not exist", ConsoleColor.Red);

                            break;
                        }
                }
            }
        }

        public static void DrawMainMenu()
        {

        }

        public static void WriteInfo(
            string message,
            ConsoleColor consoleColor = ConsoleColor.Green)
        {
            Console.ForegroundColor = consoleColor;

            Console.WriteLine(message);

            Console.ResetColor();
        }
    }
}
