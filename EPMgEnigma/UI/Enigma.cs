﻿using EPMgEnigma.Services.Auth;
using EPMgEnigma.Common;
using System;

namespace EPMgEnigma.UI
{
    public class Enigma : IEnigma
    {
        private readonly IAuthService authService;

        public Enigma(
            IAuthService authService)
        {
            this.authService = authService;
        }

        public void LogIn()
        {
            string login = ConsoleHelper.GetLogin();

            var response = authService.IsLoginInDatabase(login);

            if (response.IsSuccessful)
            {
                if (response.Data)
                {
                    string password = ConsoleHelper.GetPassword();

                    var user = authService.LogIn(login, password);

                    if (user != null)
                    {
                        Console.Clear();

                        MainMenu();
                    }
                    else
                    {
                        ConsoleHelper.WriteInfo("Wrong password. Try again", ConsoleColor.Red);

                        Console.Clear();

                        LogIn();
                    }
                }
            }
        }

        public void MainMenu()
        {
            ConsoleHelper.WriteInfo("Enter contact name to start conversation with him", ConsoleColor.Blue);

            // TODO export a list of usernames

            string contact = Console.ReadLine();
        }

        public void SignUp()
        {
            string login = ConsoleHelper.GetLogin();

            var response = authService.IsLoginInDatabase(login);

            if (response.IsSuccessful)
            {
                if (response.Data)
                {
                    ConsoleHelper.WriteInfo("User with that login is exist.", ConsoleColor.Red);
                }
                else
                {
                    string password = ConsoleHelper.GetPassword();

                    authService.SignUp(login, password);
                }
            }
            else
            {
                ConsoleHelper.WriteInfo(/*response.ErrorText*/"Fuck", ConsoleColor.Red);
            }

            Console.Clear();

            ConsoleHelper.WriteInfo("And now, please LogIn", ConsoleColor.Blue);
        }

        public void StartMenu()
        {
            ConsoleHelper.WriteInfo("Welcome to console app", ConsoleColor.Blue);

            ConsoleHelper.WriteInfo($"Press a key comand to continue: LogIn({(int)KeyImput.LogIn}) or SignUp({(int)KeyImput.SignUp})");

            KeyImput key = ConsoleHelper.DrawStartMenuKeyCommand();

            switch (key)
            {
                case KeyImput.LogIn:
                    LogIn();
                    break;
                case KeyImput.SignUp:
                    SignUp();
                    LogIn();
                    break;
                default:
                    break;
            }
        }
    }
}
