﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EPMgEnigma.UI
{
    public enum KeyImput
    {
        LogIn,
        SignUp,
    }

    public interface IEnigma
    {
        void StartMenu();
        
        void LogIn();
        
        void SignUp();

        void MainMenu();
    }
}
